# 7. CKA Pratice Exam Part 7 ACG : PV et PVC

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

1. Créer un volume persistant

2. Créer un pod qui utilise le PersistentVolume pour le stockage

3. Développez la PersistentVolumeClaim


# Contexte

Cet atelier propose des scénarios pratiques pour vous aider à vous préparer à l'examen Certified Kubernetes Administrator (CKA). Des tâches à accomplir vous seront présentées ainsi que des serveurs et/ou un cluster Kubernetes existant pour les accomplir. Vous devrez utiliser vos connaissances de Kubernetes pour mener à bien les tâches fournies, un peu comme vous le feriez sur le terrain réel. Examen CKA. Bonne chance!

Cette question utilise le acgk8scluster. Après vous être connecté au serveur d'examen, passez au contexte correct avec la commande kubectl config use-context acgk8s.

Chacun des objectifs représente une tâche que vous devrez accomplir en utilisant le cluster et les serveurs disponibles. Lisez attentivement chaque objectif et accomplissez la tâche spécifiée.

Pour certains objectifs, vous devrez peut-être vous connecter à d'autres nœuds ou serveurs à partir du serveur d'examen. Vous pouvez le faire en utilisant le nom d'hôte/nom de nœud (c'est-à-dire ssh acgk8s-worker1).

Remarque : Vous ne pouvez pas sshaccéder à un autre nœud, ni utiliser kubectlpour vous connecter au cluster, à partir d'un nœud autre que le nœud racine. Une fois que vous avez terminé les tâches nécessaires sur un serveur, assurez-vous de exitrevenir au nœud racine avant de continuer.

Si vous devez assumer les privilèges root sur un serveur, vous pouvez le faire avec sudo -i.

Vous pouvez exécuter le script de vérification situé à /home/cloud_user/verify.shtout moment pour vérifier votre travail !


>![Alt text](img/image.png)

# Application



## Étape 1 : Connexion au Serveur

1. Connectez-vous au serveur à l'aide des informations d'identification fournies :

```sh
ssh cloud_user@<PUBLIC_IP_ADDRESS>
```

2. Remarque : lorsque vous copiez et collez du code dans Vim à partir du guide de laboratoire, entrez d'abord `:set paste` (puis passez en mode insertion en appuyant sur `i`) pour éviter d'ajouter des espaces et des hachages inutiles. Pour enregistrer et quitter le fichier, appuyez sur `Échap` suivi de `:wq`. Pour quitter le fichier sans enregistrer, appuyez sur `Échap` suivi de `:q!`.

## Étape 2 : Créer un Volume Persistant

1. Passez au contexte `acgk8s` :

```sh
kubectl config use-context acgk8s
```

2. Créez un fichier YAML nommé `localdisk.yml` :

```sh
vim localdisk.yml
```

3. Dans le fichier, ajoutez ce qui suit :

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: localdisk
reclaimPolicy: Retain
provisioner: kubernetes.io/no-provisioner
allowVolumeExpansion: true
```


Ref doc : 

https://kubernetes.io/docs/concepts/storage/storage-classes/#local

https://kubernetes.io/docs/concepts/storage/storage-classes/#storageclass-objects

https://kubernetes.io/docs/concepts/storage/storage-classes/#allow-volume-expansion

4. Enregistrez le fichier :

   - Appuyez sur `ESC`
   - Tapez `:wq` et appuyez sur `Entrée`

5. Créez la classe de stockage à l'aide du fichier YAML :

```sh
kubectl create -f localdisk.yml
```

>![Alt text](img/image-1.png)

6. Créez un fichier YAML nommé `host-storage-pv.yml` :

```sh
vim host-storage-pv.yml
```

7. Dans le fichier, ajoutez ce qui suit :

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: host-storage-pv
  namespace: auth
spec:
  storageClassName: localdisk
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/etc/data"
  persistentVolumeReclaimPolicy: Recycle
```

Ref doc : 

https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/#create-a-persistentvolume

8. Enregistrez le fichier :

   - Appuyez sur `ESC`
   - Tapez `:wq` et appuyez sur `Entrée`

9. Créez le volume persistant :

```sh
kubectl create -f host-storage-pv.yml
```

10. Vérifiez l'état du volume :

```sh
kubectl get pv host-storage-pv
```

>![Alt text](img/image-2.png)

11. Vérifiez l'état des objectifs de l'examen :

```sh
./verify.sh
```

## Étape 3 : Créer un Pod qui Utilise le PersistentVolume pour le Stockage

1. Créez un fichier YAML nommé `host-storage-pvc.yml` :

```sh
vim host-storage-pvc.yml
```

2. Dans le fichier, collez ce qui suit :

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: host-storage-pvc
  namespace: auth
spec:
  storageClassName: localdisk
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi
```


3. Enregistrez le fichier :

   - Appuyez sur `ESC`
   - Tapez `:wq` et appuyez sur `Entrée`

4. Créez le PersistentVolumeClaim dans le même espace de noms que le pod :

```sh
kubectl create -f host-storage-pvc.yml
```


5. Vérifiez l’état de PersistentVolumeClaim :

```sh
kubectl get pv
```

6. Vérifiez que la revendication est liée au volume :

```sh
kubectl get pvc -n auth
```

>![Alt text](img/image-4.png)


7. Créez un fichier YAML nommé `pv-pod.yml` :

```sh
vim pv-pod.yml
```

8. Dans le fichier, collez ce qui suit :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pv-pod
  namespace: auth
spec:
  containers:
    - name: busybox
      image: busybox
      command: [sh -c while true; do echo success > /output/output.log; sleep 5; done]
      volumeMounts:
      - mountPath: "/output"
        name: pv-storage
  volumes:
    - name: pv-storage
      persistentVolumeClaim:
        claimName: host-storage-pvc
```


Ref doc : 

https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/#create-a-pod

https://kubernetes.io/docs/concepts/storage/persistent-volumes/#claims-as-volumes


https://kubernetes.io/docs/concepts/workloads/pods/#pod-templates

9. Enregistrez le fichier :

   - Appuyez sur `ESC`
   - Tapez `:wq` et appuyez sur `Entrée`

10. Créez le pod :

```sh
kubectl create -f pv-pod.yml
```

11. Vérifiez l'état des objectifs de l'examen :

```sh
./verify.sh
```

>![Alt text](img/image-5.png)

## Étape 4 : Développez la PersistentVolumeClaim

1. Modifier `host-storage-pvc` :

```sh
kubectl edit pvc host-storage-pvc -n auth
```

2. Sous `spec`, modifiez la valeur de `storage` en `200Mi` .

3. Enregistrez le fichier :

   - Appuyez sur `ESC`
   - Tapez `:wq` et appuyez sur `Entrée`

4. Vérifiez l'état des objectifs de l'examen :

```sh
./verify.sh
```

En suivant ces étapes, vous aurez créé un volume persistant, un pod qui utilise ce volume pour le stockage, et agrandi la PersistentVolumeClaim pour ce pod.